/*
 * pispi_8ai.c
 *
 * Copyright 2016  <pi@raspberrypi>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

// Define Pins as used by Pi-SPi modules
#define CS_2AO		4
#define CS_8DI		17
#define CS_SPARE2	27
#define CS_SPARE1	22
#define CS_LEDS		18
#define CS_LCD1		23
#define CS_LCD2		24
#define CS_8KO		8
#define CS_8AI		7
#define DIR_RS485	25

#define mA_20 		3723	// 20 mA into 150Ohms = 3 Vdc / 3.3 Vdc full scale * 4096 Counts

// Prototypes
void Initialize_Pi_Hardware(void);
void Update_Analog(unsigned char channel);

// Variables
unsigned AN_AD[8];
float AN_mA[8];

// main
int main(void) {

	int i;

	wiringPiSetupGpio();
  	Initialize_Pi_Hardware();
 	digitalWrite(CS_8AI, HIGH);

	while(1)
	{
		Update_Analog(0);
		Update_Analog(1);
		Update_Analog(2);
		Update_Analog(3);
		Update_Analog(4);
		Update_Analog(5);
		Update_Analog(6);
		Update_Analog(7);

		for(i = 0; i < 8; i++) {
			printf("Channel AN%d = %d ADC, %5.2f mA \n", (i+1), AN_AD[i], AN_mA[i]);
		}

		printf("\n");
		delay(1000);
	}

	return 0;
}

void Update_Analog(unsigned char channel) {

	unsigned int  adc, input;
	unsigned char buf[3];

	wiringPiSPISetup(1, 100000);
	input = 0x0600 | (channel << 6);

	buf[0] = (input >> 8) & 0xff;
	buf[1] = input & 0xff;
	buf[2] = 0;

	digitalWrite(CS_8AI, LOW);
	wiringPiSPIDataRW(1,buf,3);
	adc = ((buf[1] & 0x0f ) << 8) | buf[2];
	digitalWrite(CS_8AI, HIGH);

	AN_AD[channel] = adc;
	AN_mA[channel] = (float) adc * 20 / mA_20;
}


void Initialize_Pi_Hardware(void) {

	// SPI CS Enable Lines
	pinMode(CS_2AO, OUTPUT);
	pinMode(CS_8DI, OUTPUT);
	pinMode(CS_SPARE2, OUTPUT);
	pinMode(CS_SPARE1, OUTPUT);
	pinMode(CS_LEDS, OUTPUT);
	pinMode(CS_LCD1, OUTPUT);
	pinMode(CS_LCD2, OUTPUT);
	pinMode(CS_8KO, OUTPUT);
	pinMode(CS_8AI, OUTPUT);
	pinMode(DIR_RS485, OUTPUT);
}

