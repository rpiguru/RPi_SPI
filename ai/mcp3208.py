"""
*** Python Driver class for Microchip's MCP3208 AD Converter ***

Copyright 2016

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS
"""

import os
import sys
import time

# Add parent directory to path for `pispi.py`.
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
from pispi import PiSPI

__version__ = 0.1


class MCP3208(PiSPI):
    """
    NOTE: Maximum frequency is 2Mz in 5V VDD
    """
    v_ref = 3.3

    def __init__(self, cs_pin=7, v_ref=3.3):                        # AI module uses GPIO7 for SPI_CS
        # Though MCP3208 has maximum clock of 2MHz, I set it as 61kHz
        # See below for more details.
        # https://www.raspberrypi.org/documentation/hardware/raspberrypi/spi/README.md
        self.v_ref = v_ref
        # PiSPI.__init__(self, cs_pin=cs_pin, speed=61000, bus=bus, device=device)
        PiSPI.__init__(self, cs_pin=cs_pin, speed=61000)

    def read_channel(self, channel):
        """
        Read channel value
        For more detail, see FIGURE 6-1 in page 18 of datasheet.
        :param channel:
        :return:
        """
        buf = 0x0600 | (channel << 6)       # 2-byte length
        cmd1 = (buf >> 8) & 0xff            # 1st byte
        cmd2 = buf & 0xff                   # 2nd byte
        resp = self.send_cmd([cmd1, cmd2, 0])
        # we just need last 12 bits
        val = ((resp[1] & 0x0f) << 8) | resp[2]
        return round(val * self.v_ref / 4096.0, 4)


if __name__ == '__main__':

    a = MCP3208()

    # The resistor value of AI board is 150ohm.
    while True:
        for i in range(8):
            volt = a.read_channel(i)
            print 'CH {}: {} V ({}mA)'.format(i, volt, round(volt/150*1000, 5))
        print
        time.sleep(1)
