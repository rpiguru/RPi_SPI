# Python Driver for Pi-SPi-8DI Raspberry Pi Digital Input Interface

https://widgetlords.myshopify.com/collections/pi-spi-series/products/pi-spi-8di-raspberry-pi-digital-input-interface

## Preparing RPi
    
- Expand file system.
    
        sudo raspi-config
        
    Select No.1 and expand file system, and reboot RPi.
    
- Update & upgrade packages.
    
        sudo apt-get update
        sudo apt-get upgrade

- Enable SPI0 bus on RPi.
    
        sudo raspi-config
    
    Go to `9(Advanced Options) -> A6(SPI)` and enable SPI bus, and reboot.

- Install dependencies

    After enabling SPI bus, we need to install some python packages.
    
        sudo pip install spidev
    
        