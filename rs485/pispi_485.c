/*
 * pispi_rs485.c
 *
 * Copyright 2016  <pi@raspberrypi>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <wiringSerial.h>

// Define Pins as used by Pi-SPi modules
#define CS_2AO		4
#define CS_8DI		17
#define CS_SPARE2	27
#define CS_SPARE1	22
#define CS_LEDS		18
#define CS_LCD1		23
#define CS_LCD2		24
#define CS_8KO		8
#define CS_8AI		7
#define DIR_RS485	25

#define MODBUS_ID_DISPLAY 	101

// Prototypes
void Initialize_Pi_Hardware(void);
void Update_Uart_Remote_Display(void);
unsigned int MB_crc16(char *p, unsigned char n);

// Variables
int fd;

// main
int main(void) {

	wiringPiSetupGpio();
  	Initialize_Pi_Hardware();
 	digitalWrite(DIR_RS485, LOW);

	while(1)
	{
		Update_Uart_Remote_Display();
		delay(1000);
	}

	return 0;
}


void Initialize_Pi_Hardware(void) {

	// SPI CS Enable Lines
	pinMode(CS_2AO, OUTPUT);
	pinMode(CS_8DI, OUTPUT);
	pinMode(CS_SPARE2, OUTPUT);
	pinMode(CS_SPARE1, OUTPUT);
	pinMode(CS_LEDS, OUTPUT);
	pinMode(CS_LCD1, OUTPUT);
	pinMode(CS_LCD2, OUTPUT);
	pinMode(CS_8KO, OUTPUT);
	pinMode(CS_8AI, OUTPUT);
	pinMode(DIR_RS485, OUTPUT);
}

// MODBUS RTU Error Checkum calculation using Polynomial
unsigned int MB_crc16(char *p, unsigned char n) {

	unsigned char i;
	unsigned int crc, poly;

	crc = 0xffff;
	poly = 0xa001;

	while(n--) {
		crc ^= *p++;

		for(i = 8; i != 0; i--) {
			if(crc&1) {
				crc = (crc>>1) ^ poly;
			}
			else {
				crc >>= 1;
			}
		}
	}

	return crc;
}

void Update_Uart_Remote_Display(void) {

	unsigned char i, index;
	char tx_buffer[97];
	unsigned int checksum;

 	// Open Serial Port and Initialize
 	//fd = serialOpen("/dev/ttyAMA0", 19200);		// for RPi < 3

 	fd = serialOpen("/dev/ttyS0", 19200);			// for RPi 3

 	if(fd < 0)
 	{
		printf("Error opening Serial Port \n");
		return;
	}

	index = 0;

	tx_buffer[index++] = 101;		// MODBUS ID for slave relay module
	tx_buffer[index++] = 16;		// Function 16 = write multiple holding registers

	tx_buffer[index++] = 0;			// Start Address High Byte
	tx_buffer[index++] = 0;			// Start Address Low Byte

	tx_buffer[index++] = 0;			// Qty Regsiters High Byte
	tx_buffer[index++] = 43;		// Qty Registers Low Byte

	tx_buffer[index++] = 84;		// Byte Count

	sprintf(&tx_buffer[index],"    Pi-SPi-RS485    \n");
	index += 20;
	sprintf(&tx_buffer[index],"   Remote Display   \n");
	index += 20;
	sprintf(&tx_buffer[index],"        Demo        \n");
	index += 20;
	sprintf(&tx_buffer[index],"     19200 Baud     \n");
	index += 20;

	tx_buffer[index++] = 0;
	tx_buffer[index++] = 0;
	tx_buffer[index++] = 0;
	tx_buffer[index++] = 0;
	tx_buffer[index++] = 0;
	tx_buffer[index++] = 0;

	checksum = MB_crc16(tx_buffer,index);

	// Checksum Low Byte, High Byte, Sent Little Endian
	tx_buffer[index++] = (unsigned char)(checksum & 0x00ff);
	tx_buffer[index++] = (unsigned char)((checksum >> 8) & 0x00ff);

	digitalWrite(DIR_RS485, HIGH);		// Set RS485 to TX
	delay(10);							// Allow lines to settle

	for(i=0; i<index; i++) {
		serialPutchar(fd, tx_buffer[i]);
	}

	delay(100);							// Allow last byte stop bit
	digitalWrite(DIR_RS485, LOW);			// Set RS485 to RX

	serialClose(fd);

}