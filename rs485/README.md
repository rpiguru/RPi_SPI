## Install dependencies

    sudo pip install modbux_tk
    
    
## Disable Serial Console before using UART.

Stop & disable current running service

    sudo systemctl stop serial-getty@ttyS0.service
    sudo systemctl disabel serial-getty@ttyS0.service
    
Open the `cmdline.txt` to remove the console. 

    sudo nano /boot/cmdline.txt
    
And remove `console=serial0,115200` and save & exit.

## Working with MAX3072
    
PiSPi RS483 module uses MAX3072 chip for RS485 communication.





