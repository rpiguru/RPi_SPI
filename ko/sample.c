/*
 * pispi_8ko.c
 *
 * Copyright 2016
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

// Define Pins as used by Pi-SPi modules
#define CS_2AO		4
#define CS_8DI		17
#define CS_SPARE2	27
#define CS_SPARE1	22
#define CS_LEDS		18
#define CS_LCD1		23
#define CS_LCD2		24
#define CS_8KO		8
#define CS_8AI		7
#define DIR_RS485	25

// Prototypes
void Initialize_Pi_Hardware(void);
void Update_Relays(unsigned char status);

// Variables
unsigned char Relay_Status;

// main
int main(void) {

	wiringPiSetupGpio();
  	Initialize_Pi_Hardware();
 	digitalWrite(CS_8KO, HIGH);

	while(1)
	{
		Relay_Status = 0x55;
		printf("Relay Status = %02x \n", Relay_Status);
		Update_Relays(Relay_Status);
		delay(1000);

		Relay_Status = 0xaa;
		printf("Relay Status = %02x \n", Relay_Status);
		Update_Relays(Relay_Status);
		delay(1000);
	}

	return 0;
}


void Update_Relays(unsigned char status) {

	unsigned char buf[2];

	buf[0] = status;

	wiringPiSPISetup(0, 100000);
	wiringPiSPIDataRW(0,buf,1);

	digitalWrite(CS_8KO, LOW);
	delay(1);
	digitalWrite(CS_8KO, HIGH);

}

void Initialize_Pi_Hardware(void) {

	// SPI CS Enable Lines
	pinMode(CS_2AO, OUTPUT);
	pinMode(CS_8DI, OUTPUT);
	pinMode(CS_SPARE2, OUTPUT);
	pinMode(CS_SPARE1, OUTPUT);
	pinMode(CS_LEDS, OUTPUT);
	pinMode(CS_LCD1, OUTPUT);
	pinMode(CS_LCD2, OUTPUT);
	pinMode(CS_8KO, OUTPUT);
	pinMode(CS_8AI, OUTPUT);
	pinMode(DIR_RS485, OUTPUT);

}


