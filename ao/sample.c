/*
 * pispi_2ao.c
 *
 * Copyright 2016
 *
 * Reference Vref = 3.3. The mA circuit is optimized to give 20 mA at a 3VDC output from the DAC.
 * Therefore, 20 mA = 3/3.3 * 4096 DA Counts = 3723 DA Counts.
 *
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

// Define Pins as used by Pi-SPi modules
#define CS_2AO		4
#define CS_8DI		17
#define CS_SPARE2	27
#define CS_SPARE1	22
#define CS_LEDS		18
#define CS_LCD1		23
#define CS_LCD2		24
#define CS_8KO		8
#define CS_8AI		7
#define DIR_RS485	25

// Prototypes
void Initialize_Pi_Hardware(void);
void Update_Analog_Output(unsigned int dac1, unsigned int dac2);

// Variables
unsigned int DAC1_Counts;
unsigned int DAC2_Counts;

// main

int main(void) {

	wiringPiSetupGpio();
  	Initialize_Pi_Hardware();
 	digitalWrite(CS_2AO, HIGH);

	while(1)
	{
		DAC1_Counts = 745;	// 745 = 4 mA
		DAC2_Counts = 3723;	// 3VDC/3.3Vref * 4096 = 3723 = 20 mA

		printf("DAC1 AD Counts Output  = %d \n", DAC1_Counts);
		printf("DAC2 AD Counts Output  = %d \n\n", DAC2_Counts);

		Update_Analog_Output(DAC1_Counts, DAC2_Counts);
		delay(1000);
	}

	return 0;
}

void Update_Analog_Output(unsigned int dac1, unsigned int dac2) {

	unsigned int  output;
	unsigned char buf[2];

	wiringPiSPISetup(1, 100000);

	// Output DAC 1

	output = 0x3000;
	output |= dac1;

	buf[0] = (output >> 8) & 0xff;
	buf[1] = output & 0xff;

	digitalWrite(CS_2AO, LOW);
	wiringPiSPIDataRW(1,buf,2);
	digitalWrite(CS_2AO, HIGH);

	delay(5);

	// Output DAC 2

	output = 0xb000;
	output |= dac2;

	buf[0] = (output >> 8) & 0xff;
	buf[1] = output & 0xff;

	digitalWrite(CS_2AO, LOW);
	wiringPiSPIDataRW(1,buf,2);
	digitalWrite(CS_2AO, HIGH);
}


void Initialize_Pi_Hardware(void) {

	// SPI CS Enable Lines
	pinMode(CS_2AO, OUTPUT);
	pinMode(CS_8DI, OUTPUT);
	pinMode(CS_SPARE2, OUTPUT);
	pinMode(CS_SPARE1, OUTPUT);
	pinMode(CS_LEDS, OUTPUT);
	pinMode(CS_LCD1, OUTPUT);
	pinMode(CS_LCD2, OUTPUT);
	pinMode(CS_8KO, OUTPUT);
	pinMode(CS_8AI, OUTPUT);
	pinMode(DIR_RS485, OUTPUT);

}


