# SPI - based communications with IO Boards for Upboard and Raspberry PI

## Board list
    
- Pi-SPI-8DI Raspberry Pi Digital Input Interface

    https://widgetlords.myshopify.com/collections/pi-spi-series/products/pi-spi-8di-raspberry-pi-digital-input-interface?variant=27975155017
    
    Chip: MCP23S08
    
- Pi-SPI-2A0 Raspberry Pi Analog Output (mA + VDC) Interface
    
    https://widgetlords.myshopify.com/collections/pi-spi-series/products/pi-spi-2a0-raspberry-pi-analog-output-ma-vdc-interface
    
    Chip: MCP4922
    
- Pi-SPI-8AI Raspberry Pi Analog Input (4 - 20 mA) Interface
    
    https://widgetlords.myshopify.com/collections/pi-spi-series/products/pi-spi-8ai-raspberry-pi-analog-input-interface
    
    Chip: MCP3208

- Pi-SPI-8KO Raspberry Pi Relay Output Interface
    
    https://widgetlords.myshopify.com/collections/pi-spi-series/products/pi-spi-8ko-raspberry-pi-relay-output-interface
    
    Chip: 74HCT595
    
- Pi-SPI-RS485 Raspberry Pi RS485 Interface
    
    https://widgetlords.myshopify.com/products/pi-spi-rs485-raspberry-pi-rs485-interface
    
## Connection Guide.

![Pi-SPi Family](img/Pi-SPi-Family.jpg)

*NOTE:* It is **highly** recommended to use `GPIO22` for `SPI_CS` of `Pi-SPI-8KO` board!

Please set `J2` jumper as `GPIO22` on the `Pi-SPI-8KO` board when using other boards together.

Otherwise conflict may occur and other boards will not work.


## Example usage.

**NOTE:** Each classes are inherited from `PiSPI` in the `pispi.py`.

So you could find the portion of code at the beginning where imports `PiSPI` class.

If `pispi.py` is located at the other directory, please don't forget to import it!


### AI - MCP3208
    
    from ai.mcp3208 import MCP3208
    import time
    
    a = MCP3208()

    # The resistor value of AI board is 150ohm.
    while True:
        for i in range(8):
            volt = a.read_channel(i)
            print 'CH {}: {} V ({}mA)'.format(i, volt, round(volt/150*1000, 5))
        print
        time.sleep(1)

### AO - MCP4922
    
    from ao.mcp4922 import MCP4922
    
    a = MCP4922()

    # Set gain
    a.set_gain(2)

    # Output analog voltage to channel A
    a.output_voltage(channel='a', volt=2.5)

    # Output analog voltage to channel B
    a.output_voltage(channel='b', volt=1.5)

### DI - MCP23S08
    
    from di.mcp23s08 import MCP23S08
    
    a = MCP23S08()

    print 'Setting all channels as INPUT'
    a.write_register(0x00, 0xFF)

    print 'Set IPOL Polarity to Invert'
    a.write_register(0x01, 0xFF)

    print 'Reading all channels'
    print a.get_values()

### KO - 74HCT595
    
    from ko.relay import Relay
    
    a = Relay()

    # Open all relays
    a.open_all()

    # Close all relays
    a.close_all()

    # Open the channel 5
    a.relay_open(5)

    # Close the channel 5
    a.relay_close(5)
    
### RS485
    
I am working on this now.